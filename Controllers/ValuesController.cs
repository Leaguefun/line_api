﻿using System.Collections.Generic;
using System.Linq;
using LineMessageApi.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.DependencyInjection;

namespace LineMessageApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ValuesController : ControllerBase
    {
        private ApplicationDBContext _dbContext;

        public ValuesController(IServiceScopeFactory serviceScopeFactory)
        {
            var _scope = serviceScopeFactory.CreateScope();
            
            _dbContext = serviceScopeFactory.CreateScope().ServiceProvider.GetRequiredService<ApplicationDBContext>();
        }

        // GET api/values
        [HttpGet]
        public ActionResult<IEnumerable<string>> Get()
        {
            return new string[] { "value1", "value2" };
        }

        [HttpGet("{szCriteria}")]
        public ActionResult<IQueryable<RejectionCodeRec>> Get(string szCriteria)
        {
            return new ActionResult<IQueryable<RejectionCodeRec>>(_dbContext.FindRecords(szCriteria));
        }
/*
        // GET api/values/5
        [HttpGet("{id}")]
        public ActionResult<string> Get(int id)
        {
            return "value";
        }
//*/
        // POST api/values
        [HttpPost]
        public void Post([FromBody] string value)
        {
        }

        // PUT api/values/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] string value)
        {
        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
