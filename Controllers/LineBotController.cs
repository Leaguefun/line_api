﻿using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using LineMessageApi.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.DependencyInjection;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using NLog;

namespace LineMessageApi.Controllers
{
    // https://api.appslause.com:35797/api/linebot
    [Produces("application/json")]
    [Route("api/[controller]")]
    public class LineBotController : Controller
    {
        private Logger _logger = NLog.LogManager.GetCurrentClassLogger();
        private ApplicationDBContext _dbContext;
        AppSettings _appsettings;


        public LineBotController(AppSettings appConfig, IServiceScopeFactory serviceScopeFactory)
        {
            _appsettings = appConfig;
            var _scope = serviceScopeFactory.CreateScope();
            _dbContext = serviceScopeFactory.CreateScope().ServiceProvider.GetRequiredService<ApplicationDBContext>();
        }

        [HttpGet]
        public IEnumerable<string> Get()
        {
            _logger.Info("In get: api/linebot");
            return new string[] { "value1", "value2" };
        }

        /// <summary>
        /// POST: api/Messages
        /// Receive a message from a user and reply to it
        /// </summary>
        [HttpPost]
        public void Post([FromBody] dynamic req)
        //       public void Post([FromBody] Events events)
        {
            try
            {
                string responseTextJson = "";
                string lineAccessToken = _appsettings.AppConfig["LineChannelAccessToken"];
                Events events = JsonConvert.DeserializeObject<Events>(req?.ToString());

                //                System.Text.Json.JsonValueKind o = req;
                //                var input = JsonConvert.DeserializeObject<Events>(req);
                //               var input = JsonConvert.SerializeObject(req);
                //               _logger.Info(input.ToString());
                //                Events events = null;// JsonConvert.DeserializeObject<Events>(req) as Events;
                if (events != null)
                {
                    string userId = events.EventList[0].Source.UserId;
                    string roomId = events.EventList[0].Source.RoomId;
                    string groupId = events.EventList[0].Source.GroupId;
                    string replyToken = events.EventList[0].ReplyToken;
                    string message = events.EventList[0].Message.Text;




                    var chatId = string.Empty;
                    if (groupId != null)
                    {
                        chatId = groupId;
                    }
                    else if (roomId != null)
                    {
                        chatId = roomId;
                    }
                    else if (userId != null)
                    {
                        chatId = userId;
                    }

                    _logger.Info(chatId.ToString());
                    var replyMessage = string.Empty;

                    _logger.Info(string.Format("UserId: {0}", chatId.ToString()));
                    _logger.Info(string.Format("ReplyToken: {0}", replyToken.ToString()));
                    _logger.Info(string.Format("Message: {0}", message.ToString()));

                    //List<string> res = _messageHandler(message);
                    List<PushMessage> res = _replyMessageHandler(message);
                    int iLoop = 0;
                    int iStep = 1;
                    int iMaxItem = 150;
                    if (res != null && res.Count() > iMaxItem)
                    {
                        iLoop = (res.Count() / iMaxItem) + (res.Count() % iMaxItem);
                        iStep = iMaxItem;
                    }
                    else
                    {
                        iLoop = 1;
                        iStep = res.Count();
                    }
                    for (int i = 0, j = 0; i < iLoop; ++i, j += iStep)
                    {
                        //_postResultToRecipent(res, j, replyToken, lineAccessToken);
                        _postResultToRecipent(res, j, iStep, userId, lineAccessToken);
                    }
                    //                    ReplyMessages _replyMessages = new ReplyMessages();
                    //                    _replyMessages.ReplyToken = replyToken;
                    //                    _replyMessages.Message = res;
                    //                    responseTextJson = JsonConvert.SerializeObject(_replyMessages);
                    //                    System.Diagnostics.Debug.WriteLine($"Response JSON = {responseTextJson}");
                    ////                    _logger.Info(string.Format("replyMessage {0}", replyMessage));

                    ////                    var messagesJson = string.Empty;

                    ////                    var jsonresult = string.Empty;

                    //                        /*
                    //                                            for (int i = 0; i < res.Count; i++)
                    //                                            {
                    //                                                var msg = res[i];
                    //                                                messagesJson += msg;
                    //                                                jsonresult += "{\"type\":\"text\",\"text\":\"" + msg + "\"}";
                    //                                                var str = i == res.Count - 1 ? "" : ",";
                    //                                                jsonresult += str;
                    //                                            }

                    //                                            jsonresult = string.Empty;
                    //                                            jsonresult += "{\"type\":\"text\",\"text\":\"" + messagesJson + "\"}";

                    //                                            responseTextJson = "{\"replyToken\":\"" + replyToken +
                    //                                                               "\",\"messages\":[" + jsonresult + "]}";
                    //                        //*/
                    //                        var httpWebRequest = (HttpWebRequest)WebRequest.Create(_appsettings.AppConfig["LineApiEndPoint"]);
                    //                    httpWebRequest.ContentType = "application/json";
                    //                    httpWebRequest.Method = "POST";
                    //                    httpWebRequest.Headers.Add("Authorization", "Bearer " + lineAccessToken);
                    //                    _logger.Info(string.Format("{0}:{1}", replyToken, responseTextJson));
                    //                    System.Diagnostics.Debug.WriteLine(string.Format("{0}:{1}", replyToken, responseTextJson));
                    //                    using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
                    //                    {
                    //                        streamWriter.Write(responseTextJson);
                    //                    }

                    //                    var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();

                    //                    using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
                    //                    {
                    //                        var result = streamReader.ReadToEnd();
                    //                    }
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex.Message);
            }
        }

        // Send reply to recipient for each 10 message 
        private bool _postResultToRecipent(
            List<PushMessage> msgs,
            int _startIndex,
            int _step,
            string _recipient,
            string _lineAccessToken)
        {
            try
            {
                PushMessages _pushMessage = new PushMessages();
                _pushMessage.To = _recipient;
                List<PushMessage> res = new List<PushMessage>();
                string paragragh = string.Empty;
                for (int i = _startIndex, j = 0; i < msgs.Count && j < _step; ++i, ++j)
                {
                    //res.Add(msgs[i]);
                    paragragh += msgs[i].Text;
                }
                res.Add(new PushMessage { Type = "text", Text = paragragh });
                _pushMessage.Message = res;
                var responseTextJson = JsonConvert.SerializeObject(_pushMessage);
                System.Diagnostics.Debug.WriteLine($"Response JSON = {responseTextJson}");
                var httpWebRequest = (HttpWebRequest)WebRequest.Create(_appsettings.AppConfig["LinePushApiEndPoint"]);
                httpWebRequest.ContentType = "application/json";
                httpWebRequest.Method = "POST";
                httpWebRequest.Headers.Add("Authorization", "Bearer " + _lineAccessToken);
                _logger.Info(string.Format("{0}:{1}", _recipient, responseTextJson));
                System.Diagnostics.Debug.WriteLine(string.Format("{0}:{1}", _recipient, responseTextJson));
                using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
                {
                    streamWriter.Write(responseTextJson);
                }

                var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();

                using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
                {
                    var result = streamReader.ReadToEnd();
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Exception found = {ex.Message}");
                return false;
            }
            return true;
        }

        private List<PushMessage> _replyMessageHandler(string message)
        {
            PushMessage returnMessage = null;
            var listMessage = new List<PushMessage>();
            //            message = message.ToLower();

            if (message.ToLower().Contains("hello"))
            {
                returnMessage = new PushMessage { Type = "text", Text = "I am Dr. Reader\n" };
                listMessage.Add(returnMessage);
            }
            else
            {
                int iNum = -1;
                Int32.TryParse(message,
                    System.Globalization.NumberStyles.AllowHexSpecifier, CultureInfo.InvariantCulture, out iNum);
//                iNum = Convert.ToInt32(message, 16);
                if (iNum > 0 || message.ToLower().StartsWith("0x"))
                {
                    string szSearch = message;
                    if (message.ToLower().StartsWith("0x"))
                        szSearch = message.Substring(2);
                    // Search by error code
                    string er = "";
                    //                    var errs = BPS_Common.ErrorCodes.Keys.Where(e => e.ToLower().Contains(szSearch));
                    var errs = _dbContext.FindRecords(szSearch);
                    if (errs != null && errs.Count() > 0)
                    {
                        listMessage.Add(new PushMessage { Type = "text", Text = $"Found {errs.Count()} match(s) :\n" });
                        foreach (var err in errs)
                        {
                            /*                            if (BPS_Common.ErrorCodes.ContainsKey(err))
                                                        {
                                                            er = BPS_Common.ErrorCodes[err];
                                                            listMessage.Add(new PushMessage { Type = "text", Text = $"{err} => {er}\n" });
                                                        }//*/
                            listMessage.Add(new PushMessage { Type = "text", Text = $"{(string.IsNullOrEmpty(err.Description) ? err.Reader_Error_Desc : err.Description)} => {err.RejectionCode},{err.Reader_Error_Code}\n" });
                        }
                    }
                }
                else
                {
                    // Search by error message
                    string er = "";
                    //var errs = BPS_Common.ErrorCodes.Values.Where(e => e.ToLower().Contains(message.ToLower()));
                    var errs = _dbContext.FindRecords(message);
                    if (errs != null && errs.Count() > 0)
                    {
                        listMessage.Add(new PushMessage { Type = "text", Text = $"Found {errs.Count()} match(s) :\n" });
                        foreach (var err in errs)
                        {
                            /*                            er = BPS_Common.ErrorCodes.FirstOrDefault(x => x.Value == err).Key;
                                                        if (!string.IsNullOrEmpty(er))
                                                        {
                                                            listMessage.Add(new PushMessage { Type = "text", Text = $"{er} => {err}\n" });
                                                        }//*/
                            listMessage.Add(new PushMessage { Type = "text", Text = $"{(string.IsNullOrEmpty(err.Description) ? err.Reader_Error_Desc : err.Description)} => {err.RejectionCode},{err.Reader_Error_Code}\n" });
                        }
                    }
                }
            }

            if (listMessage.Count() == 0)
            {
                returnMessage = new PushMessage { Type = "text", Text = $"Sorry, can't found any result with '{message}'\n" };
                listMessage.Add(returnMessage);
            }
            return listMessage;
        }

        private List<PushMessage> _replyMessageHandlerStatic(string message)
        {
            PushMessage returnMessage = null;
            var listMessage = new List<PushMessage>();
            //            message = message.ToLower();

            if (message.ToLower().Contains("hello"))
            {
                returnMessage = new PushMessage { Type = "text", Text = "I am Dr. Reader\n" };
                listMessage.Add(returnMessage);
            }
            else
            {
                int iNum = -1;
                Int32.TryParse(message,
                    System.Globalization.NumberStyles.AllowHexSpecifier, CultureInfo.InvariantCulture, out iNum);
                //                iNum = Convert.ToInt32(message, 16);
                if (iNum > 0 || message.ToLower().StartsWith("0x"))
                {
                    string szSearch = message;
                    if (message.ToLower().StartsWith("0x"))
                        szSearch = message.Substring(2);
                    // Search by error code
                    string er = "";
                    var errs = BPS_Common.ErrorCodes.Keys.Where(e => e.ToLower().Contains(szSearch));
                    if (errs != null && errs.Count() > 0)
                    {
                        listMessage.Add(new PushMessage { Type = "text", Text = $"Found {errs.Count()} match :\n" });
                        foreach (var err in errs)
                        {
                            if (BPS_Common.ErrorCodes.ContainsKey(err))
                            {
                                er = BPS_Common.ErrorCodes[err];
                                listMessage.Add(new PushMessage { Type = "text", Text = $"{err} => {er}\n" });
                            }
                        }
                    }
                }
                else
                {
                    // Search by error message
                    string er = "";
                    var errs = BPS_Common.ErrorCodes.Values.Where(e => e.ToLower().Contains(message.ToLower()));
                    if (errs != null && errs.Count() > 0)
                    {
                        listMessage.Add(new PushMessage { Type = "text", Text = $"Found {errs.Count()} match :\n" });
                        foreach (var err in errs)
                        {
                            er = BPS_Common.ErrorCodes.FirstOrDefault(x => x.Value == err).Key;
                            if (!string.IsNullOrEmpty(er))
                            {
                                listMessage.Add(new PushMessage { Type = "text", Text = $"{er} => {err}\n" });
                            }
                        }
                    }
                }
            }

            if (listMessage.Count() == 0)
            {
                returnMessage = new PushMessage { Type = "text", Text = $"Sorry, there is no match result from your search '{message}'\n" };
                listMessage.Add(returnMessage);
            }
            return listMessage;
        }

        private List<string> _messageHandler(string message)
        {
            string returnMessage;
            var listMessage = new List<string>();
            message = message.ToLower();

            if (message.ToLower().Contains("hello"))
            {
                returnMessage = "I am Dr. Reader\n";
                listMessage.Add(returnMessage);
            }
            else
            {
                if (message.ToLower().StartsWith("0x"))
                {
                    // Search by error code
                    string er = "";
                    var errs = BPS_Common.ErrorCodes.Keys.Where(e => e.Contains(message.Substring(2)));
                    if (errs != null && errs.Count() > 0)
                    {
                        listMessage.Add($"Found {errs.Count()} match :\n");
                        foreach (var err in errs)
                        {
                            if (BPS_Common.ErrorCodes.ContainsKey(err))
                            {
                                er = BPS_Common.ErrorCodes[err];
                                listMessage.Add($"{err} => {er}\n");
                            }
                        }
                    }
                }
                else
                {
                    // Search by error message
                    string er = "";
                    var errs = BPS_Common.ErrorCodes.Values.Where(e => e.Contains(message));
                    if (errs != null && errs.Count() > 0)
                    {
                        listMessage.Add($"Found {errs.Count()} match :\n");
                        foreach (var err in errs)
                        {
                            er = BPS_Common.ErrorCodes.FirstOrDefault(x => x.Value == er).Key;
                            if (!string.IsNullOrEmpty(er))
                            {
                                listMessage.Add($"{er} => {err}\n");
                            }
                        }
                    }
                    else
                    {
                        returnMessage = $"Sorry, there is no match result from your search '{message}'\n";
                        listMessage.Add(returnMessage);
                    }
                }
            }
            return listMessage;
        }
    }
}