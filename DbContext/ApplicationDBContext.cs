﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using LineMessageApi.Models;

namespace LineMessageApi
{
    public class ApplicationDBContext : DbContext
    {
        public DbSet<RejectionCodeRec> RejectionCodeRecs { get; set; }  

        public ApplicationDBContext(DbContextOptions<ApplicationDBContext> options) : base(options)
        {
            Console.WriteLine("ApplicationDBContext constructor created");
        }

        public IQueryable<RejectionCodeRec> FindRecords(string szCriteria)
        {
            long i = -1;
            string sz = szCriteria.ToLower();
            string sz_ = $"{sz}_";
            if (Int64.TryParse(szCriteria, out i))
            {
                return RejectionCodeRecs.Where(p => (p.RejectionCode.Contains(i.ToString()) ||
                    p.Reader_Error_Code.Contains(i.ToString())));
            }
            else
            {
                //string str = $"0x{(sz.Length < 8 ? sz.PadLeft(8, '0') : sz)}";
                IQueryable<RejectionCodeRec> recs = null;
                try
                {
                    i = Int64.Parse(sz, System.Globalization.NumberStyles.HexNumber);
                    recs = RejectionCodeRecs.Where(p => p.Reader_Error_Code.ToLower().Contains(sz));
                }
                catch (Exception ex)
                {
                    System.Diagnostics.Trace.WriteLine($"Exception = {ex.Message}");
                }
                if (recs == null || ( recs != null && recs.Count() == 0))
                {
                    recs = RejectionCodeRecs.Where(p => (p.Description.ToLower().Contains(sz) ||
                    p.Reader_Error_Desc.ToLower().Contains(sz)));
                    if (recs == null || (recs != null && recs.Count() == 0))
                    {
                        recs = RejectionCodeRecs.Where(p => (p.Description.ToLower().Contains(sz_) ||
                        p.Reader_Error_Desc.ToLower().Contains(sz_)));
                    }
                }
                return recs;
            }
        }
    }
}
