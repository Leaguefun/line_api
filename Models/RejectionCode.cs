﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;

namespace LineMessageApi.Models
{
    [Table("tbRejectionCodes")]
    public class RejectionCodeRec
    {
        [Key]
        [Required]
        public int ID { get; set; }

        public string RejectionCode { get; set; }
        public string Description { get; set; }
        public string Reader_Error_Desc { get; set; }
        public string Reader_Error_Code { get; set; }
    }
}
