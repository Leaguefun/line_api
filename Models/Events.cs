﻿using System.Collections.Generic;
using Newtonsoft.Json;

namespace LineMessageApi.Models
{
    public class Events
    {
        [JsonProperty("destination")]
        public string Destination { get; set; }

        [JsonProperty("events")]
        public List<EventsList> EventList { get; set; }
    }
}
