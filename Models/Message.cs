﻿using Newtonsoft.Json;

namespace LineMessageApi.Models
{
    public class Message
    {
        [JsonProperty("id")]
        public string ID { get; set; }
        [JsonProperty("type")]
        public string Type { get; set; }
        [JsonProperty("text")]
        public string Text { get; set; }
    }
}