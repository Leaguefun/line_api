﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace LineMessageApi.Models
{
    public class PushMessages
    {
        [JsonProperty("to")]
        public string To { get; set; }

        [JsonProperty("messages")]
        public List<PushMessage> Message { get; set; }
    }

    public class PushMessage
    {
        [JsonProperty("type")]
        public string Type { get; set; }
        [JsonProperty("text")]
        public string Text { get; set; }
    }
}
