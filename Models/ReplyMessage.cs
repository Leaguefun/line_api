﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace LineMessageApi.Models
{
    public class ReplyMessages
    {
        [JsonProperty("replyToken")]
        public string ReplyToken { get; set; }

        [JsonProperty("messages")]
        public List<PushMessage> Message { get; set; }
    }

    public class ReplyMessage
    {
        [JsonProperty("type")]
        public string Type { get; set; }
        [JsonProperty("text")]
        public string Text { get; set; }
    }
}
